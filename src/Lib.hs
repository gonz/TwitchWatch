{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Lib (defaultClientID, getJSONData, printStreams, onlineChannels) where

import           Config
import           Control.Lens
import           Data.Aeson
import           Data.ByteString (ByteString)
import qualified Data.Set        as Set
import           Data.Text       (Text)
import qualified Data.Text       as Text
import           Network.Wreq
import           Text.Printf
import           Types

defaultClientID :: ByteString
defaultClientID = "jzkbprff40iqj646a697cyrvl0zt2m6"

requestUrl :: String
requestUrl = "https://api.twitch.tv/kraken/streams"

getJSONData :: String -> [(Text, Text)] -> IO (Maybe Streams)
getJSONData url extraParams =
  do
    let step acc (hName, hData) = acc & header hName .~ hData
    let requestHeaders = [("Client-ID", [defaultClientID])]
    let opts = foldl step defaults requestHeaders
    let paramStep acc (pName, pData) = acc & param pName .~ [pData]
    let withParams = foldl paramStep opts extraParams
    r <- getWith withParams url
    let body = r ^. responseBody
    let decoded = eitherDecode body

    case decoded of
      (Right good) ->
        return (Just good)
      _ ->
        return Nothing

onlineChannels :: IO (Maybe Streams)
onlineChannels =
  do
    favs <- favorites
    getJSONData requestUrl [("channel", Text.intercalate "," (Set.elems favs))]

printStreams :: Streams -> IO ()
printStreams ss = sequence_ $ map printStream (ss ^. streams)

printStream :: Stream -> IO ()
printStream s =
  putStrLn $ printf "%s [%s] (%s)" url status game
  where
    url = s ^. channel . channelUrl
    status = s ^. channel . channelStatus
    game = s ^. channel . channelGame
