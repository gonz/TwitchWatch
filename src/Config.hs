{-# LANGUAGE OverloadedStrings #-}

module Config (favorites, writeFavorites, addFavorite, removeFavorite) where

import           Data.Set         (Set)
import qualified Data.Set         as Set
import           Data.Text        (Text)
import           System.Directory

favoritesFile :: IO FilePath
favoritesFile =
  getXdgDirectory XdgConfig "TwitchWatch/favorites"

favorites :: IO (Set Text)
favorites =
  do
    confFile <- favoritesFile
    d <- readFile confFile
    let favoritesSet = read d :: Set Text
    return favoritesSet

writeFavorites :: Set Text -> IO ()
writeFavorites favs =
  do
    confFile <- favoritesFile
    writeFile confFile (show favs)

-- Why evaluate favoritesSet before calling writes in the below functions?
-- Otherwise the file will be busy, as favoritesSet will be lazily
-- evaluated and haskell will try to read it while writing the file

addFavorite :: Text -> IO ()
addFavorite fav =
  do
    confFile <- favoritesFile
    d <- readFile confFile
    let favoritesSet = read d :: Set Text
    favoritesSet `seq` writeFavorites $ Set.insert fav favoritesSet

removeFavorite :: Text -> IO ()
removeFavorite fav =
  do
    confFile <- favoritesFile
    d <- readFile confFile
    let favoritesSet = read d :: Set Text
    favoritesSet `seq` writeFavorites $ Set.delete fav favoritesSet
