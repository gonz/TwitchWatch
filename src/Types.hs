{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Types where

import           Control.Lens
import           Data.Aeson
import           Data.Aeson.TH
import qualified Data.Char     as Char

data Preview =
  Preview { _small    :: String,
            _medium   :: String,
            _large    :: String,
            _template :: String
          }
  deriving (Show)
$(deriveJSON defaultOptions
   { fieldLabelModifier = (map Char.toLower . drop 1) } ''Preview)
makeLenses ''Preview

data Channel =
  Channel { _channelMature               :: Bool,
            _channelStatus               :: String,
            _channelBroadcaster_language :: String,
            _channelDisplay_name         :: String,
            _channelGame                 :: String,
            _channelLanguage             :: String,
            _channel_id                  :: Int,
            _channelName                 :: String,
            _channelCreated_at           :: String,
            _channelUpdated_at           :: String,
            _channelLogo                 :: String,
            _channelPartner              :: Bool,
            _channelUrl                  :: String,
            _channelViews                :: Int,
            _channelFollowers            :: Int
          }
  deriving (Show)
$(deriveJSON defaultOptions
   { fieldLabelModifier = (map Char.toLower . drop 8) } ''Channel)
makeLenses ''Channel

data Stream =
  Stream { __id          :: Int,
           _game         :: String,
           _viewers      :: Int,
           _created_at   :: String,
           _video_height :: Int,
           _average_fps  :: Int,
           _delay        :: Int,
           _is_playlist  :: Bool,
           _preview      :: Preview,
           _channel      :: Channel
         }
  deriving (Show)
$(deriveJSON defaultOptions
   { fieldLabelModifier = (map Char.toLower . drop 1) } ''Stream)
makeLenses ''Stream

data Streams =
  Streams { _streams :: [Stream] }
  deriving (Show)
makeLenses ''Streams

$(deriveJSON defaultOptions
   { fieldLabelModifier = (map Char.toLower . drop 1) } ''Streams)
