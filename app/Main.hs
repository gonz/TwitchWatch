module Main where

import Lib
import Config
import System.Environment (getArgs)
import qualified Data.Text as Text

main :: IO ()
main =
  do
    args <- getArgs
    case args of
      ["add", fav] ->
        addFavorite $ Text.pack fav
      ["remove", fav] ->
        removeFavorite $ Text.pack fav
      _ ->
        do
          channels <- onlineChannels
          case channels of
            Nothing ->
              print "No data"
            Just ss ->
              printStreams ss
