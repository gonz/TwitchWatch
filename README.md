# TwitchWatch

## Installation
To install the program, it's recommended to use [stack](http://haskellstack.org):

`$ cd TwitchWatch; stack install && twitchwatch add arteezy && twitchwatch`

The above line will install TwitchWatch, add a stream to check for and then
check for that stream. Do note that the `&&` notation won't work in Fish shell.
 In Fish you'd have to use the following:
 
`$ cd TwitchWatch; stack install; and twitchwatch add arteezy; and twitchwatch`
 
## Commands
The available commands are `add` and `remove`. To only get a listing of the
streams you want to check for, run the application without any arguments.

What needs to be supplied to `add` and `remove` is the channel name (username) of
the stream on Twitch, i.e. 'esl_csgo' for ESL's CSGO stream.
